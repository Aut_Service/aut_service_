package com.example.auth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeFacade {

    @GetMapping("/home")
    public boolean home(){
        return true;
    }
}
